# Kernel User Watchfiles

This repository contains the user yaml files which are cloned by
the RHEL Kernel Subsystems Gitlab webhook. Whenever an MR for the
RHEL Kernel changes some files listed in a user's yaml file here
the hook will add a comment to the MR notifying the user with the
@ syntax.

## Using

Create a file under this project's users/ directory whose name
matches your Gitlab username. The format must be valid yaml.

The yaml must be a simple associative array where the keys are
the RHEL release to monitor activity in and the values are the
files to monitor for. The special key 'all' indicates files to
monitor across all releases.

## Examples

Check the existing files under the users/ directory for examples.

The paths in the yaml can point to specific files or directories
(ending in a forward slash).

Wildcards are handled as described in the [RHMAINTAINERS] file.

[RHMAINTAINERS]: https://gitlab.com/cki-project/kernel-ark/-/blob/os-build/redhat/rhdocs/MAINTAINERS/RHMAINTAINERS#L26
